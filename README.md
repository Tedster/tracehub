# TraceHub

A mirrorable collection of apitraces to assist in debugging compatibility layers or API implementations.

## Getting apitrace
[``apitrace``](https://github.com/apitrace/apitrace) is a program used to, essentially, record the series of commands game/app gives to an api, such as d3d9 and be able to play them back and step-through. This is sometimes much faster and easier than launching a game, and also helps to debug issues in compatibility layers where the game is not owned by a developer or native/wined3d takes a different path of execution.

### Download apitrace
The freedesktop site for apitrace is usually incredibly slow for downloading it, there is a download mirror here:

<https://release.froggi.es/apitrace/>

## How to Use
You can take a trace by taking an apitrace wrapper DLL and putting it next to the game. If you are using Wine, make sure that dll is set to `native, builtin` in winecfg.

You can then upload this trace to TraceHub to share with others, or for them to access it.

You can playback these traces with apitrace's d3dretrace or other tools. You can then verify against native (or sometimes obviously) if there are errors in an API's implementation.
## Formatting
### Trace
Your trace should be placed in the `traces/{api}` folder.

Your trace should be in .tar.xz format.

Your trace should be named as such:
`{Game Name} ({Platform} {AppID}) [{Renderer}].tar.xz`

Eg.

`KoA Reckoning (Steam 102500) [wined3d].tar.xz`

In cases where there is no Platform, the section in brackets may be omitted.
### Commit
Your commit should follow the form
``[trace] {Game Name}``
